var colour = [ 'red', 'yellow', 'green', 'blue' ]
var value = [ 0, 1, 2, 3, 4, 5, 6, 7, 8 , 9, 20, 20, 20 ]
var image = [ '00', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12' ]
var UnoDeck = function(imageBase) {
	if (!!!imageBase)
		imageBase = '/images/uno_deck';
	this.deck = []
	for (var i = 0; i < colour.length; i++)
		this.deck.push({
			colour: colour[i],
			value: 0,
			image: imageBase + '/c' + i + '_00.png'
		})
	for (var x = 0; x < 2; x++)
		for (var i = 0; i < colour.length; i++) {
			for (var j = 1; j < value.length; j++)
				this.deck.push({
					colour: colour[i],
					value: value[j],
					image: imageBase + '/c' + i + '_' + image[j] + '.png'
				})
		}
	for (var i = 0; i < 4; i++) {
		this.deck.push({
			colour: 'any',
			value: 50,
			image: imageBase + '/c4_00.png'
		});
		this.deck.push({
			colour: 'any',
			value: 50,
			image: imageBase + '/c4_01.png'
		});
	}
}

UnoDeck.prototype.shuffle = function(cycles) {
	if (!cycles)
		cycles = 10;

	for (var i = 0; i < cycles; i++)
		for (var j = 0; j < this.deck.length; j++) {
			var swapIdx = Math.floor(Math.random() * this.deck.length);
			var swapCard = this.deck[swapIdx]
			this.deck[swapIdx] = this.deck[j]
			this.deck[j] = swapCard;
		}
}
UnoDeck.prototype.take = function(num) {
	if (!num)
		num = 1;
	var toReturn = []
	for (var i = 0; i < num; i++)
		toReturn.push(this.deck.shift())
	return (toReturn);
}

module.exports = UnoDeck;

(function() {
	var TableApp = angular.module("TableApp", []);

	var TableCtrl = function($scope, $http) {
		var tableCtrl = this;

		tableCtrl.gameId = "No started";
		tableCtrl.started = false;
		tableCtrl.socket = null;
		tableCtrl.topCard = "";
		tableCtrl.cardCount = 0;

		tableCtrl.createGame = function() {
			$http.post("/game")
				.then(function(result) {
					tableCtrl.gameId = result.data.gameId;
					tableCtrl.started = true;
				});
		}

		tableCtrl.startGame = function() {
			tableCtrl.socket = new WebSocket("ws://localhost:3000/game/" 
					+ tableCtrl.gameId + "?name=TABLE");
			tableCtrl.socket.onmessage = function(evt) {
				$scope.$apply(function() {
					var data = JSON.parse(evt.data);
					console.log("table data = ", data);
					if ('drop' == data.cmd) {
						tableCtrl.topCard = data.card;
					} else if ('init' == data.cmd) {
						tableCtrl.topCard = data.setup.topCard;
						tableCtrl.cardCount = data.setup.cardCount;
					}
				});
			}
		}
	}

	TableApp.controller("TableCtrl", ["$scope", "$http", TableCtrl])
})();

(function() {
	var PlayerApp = angular.module("PlayerApp", []);

	var PlayerCtrl = function($scope) {
		var playerCtrl = this;
		playerCtrl.name = "";
		playerCtrl.gameId = "";
		playerCtrl.socket = null;
		playerCtrl.hand = [];

		playerCtrl.joinGame = function() {
			playerCtrl.socket = new WebSocket("ws://localhost:3000/game/" 
					+ playerCtrl.gameId + "?name=" + playerCtrl.name);
			playerCtrl.socket.onmessage = function(event) {
				$scope.$apply(function() {
					playerCtrl.hand = JSON.parse(event.data);
					console.log("hand = ", playerCtrl.hand);
				});
			}
		}

		playerCtrl.dropCard = function($index) {
			var cmd = { cmd: "drop", card:  playerCtrl.hand[$index] }
			playerCtrl.socket.send(JSON.stringify(cmd));
			playerCtrl.hand.splice($index, 1);
		}
	}

	PlayerApp.controller("PlayerCtrl", ["$scope", PlayerCtrl]);
})();

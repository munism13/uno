const path = require('path');
const uuid = require('uuid/v4');
const express = require('express');
const expressWS = require('express-ws');
const bodyParser = require('body-parser');

const UnoDeck = require('./libs/uno');

const app = express();
const appWS = expressWS(app);

const games = { };

app.use(bodyParser.json());

app.post("/game", function(req, resp) {
	const gid = uuid().substring(0, 8);
	games[gid] = {
		gameId: gid,
		deck: null,
		table: null,
		players: []
	}
	console.info("game = ", games[gid]);
	resp
		.status(201)
		.json({gameId: gid});
});

app.ws("/game/:gid", function(ws, req) {
	const name = req.query.name;
	const gameId = req.params.gid;
	var currGame = games[gameId];

	//Table initialization
	if ("TABLE" == name) {
		//Setup the game
		//{ gameId: abc123, deck: null, players: [ws, ws, ws] }
		currGame.deck = new UnoDeck();
		currGame.deck.shuffle(20);
		for (var i in currGame.players) {
			var hand = currGame.deck.take(7);
			currGame.players[i].send(JSON.stringify(hand));
		}
		currGame.table = ws;
		currGame.table.on('message', function(msg) {
			//{ cmd: drop }
		});
		const initalTable = {
			topCard: currGame.deck.take()[0],
			cardCount: currGame.deck.length
		}
		const cmd = { cmd: 'init', setup: initalTable }
		currGame.table.send(JSON.stringify(cmd));
		return;
	}

	//Player initialization
	ws.playerName = name;
	ws.on('message', function(msg) {
		console.log(">>> got message = ", msg);
		var cmd = JSON.parse(msg);
		if ('drop' == cmd.cmd) {
			console.log(">>> retransmit the message");
			currGame.table.send(msg);
			return;
		}
	});

	games[gameId].players.push(ws);
});


app.use(express.static(path.join(__dirname, "public")));
app.use(express.static(path.join(__dirname, "bower_components")));

const port = process.env.APP_PORT || 3000;
app.listen(port, function() {
	console.info("Application started on port %d at %s"
			, port, new Date());
});
